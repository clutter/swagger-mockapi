"use strict";

const util = require("util");
const express = require("express");
const swagger = require("swagger-express-middleware");
const Middleware = swagger.Middleware;
const MemoryDataStore = swagger.MemoryDataStore;

let app = express();
let middleware = new Middleware(app);
let tmpDB = new MemoryDataStore();

function errorHandler(err, req, res, next) { // eslint-disable-line no-unused-vars
    res.status(err.status);
    res.type("html");
    res.send(util.format("<html><body><h1>%d Error!</h1><p>%s</p></body></html>", err.status, err.message));
}

function runMockMiddleware(swaggerFile, callback) {
    middleware.init(swaggerFile, (mErr) => {
        if (!mErr) {
            app.use(
                middleware.metadata(),
                middleware.parseRequest({
                    "json": {
                        "limit": "100kb"
                    }
                }),
                middleware.CORS(), // eslint-disable-line new-cap
                middleware.files(),
                middleware.parseRequest(),
                middleware.validateRequest(),
                middleware.mock(tmpDB),
                errorHandler
            );
        }
        callback(mErr, app);
    });
}

function mock(swaggerFile, callback) {
    runMockMiddleware(swaggerFile, callback);
}

module.exports = mock;
