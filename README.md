# swagger-mockapi

API Mocking for Swagger OpenAPI Spec

## Overview

This module provides API mocking utility based on [Swagger/OpenAPI](https://www.openapis.org/) specification. Currently, [version-2.x](https://github.com/OAI/OpenAPI-Specification/blob/master/versions/2.0.md) of the specification is supported.

## Installation

```bash
$ npm install -g swagger-mockapi
```

## Usage

```bash
$ swagger-mockapi -h
Usage: index [options]

Options:
  -V, --version              output the version number
  -s, --spec [swagger.yaml]  Swagger YAML-file (default: "swagger.yaml")
  -p, --port [port]          Server port (default: 8080)
  -h, --help                 output usage information
Notes:
- Supports Swagger/OpenAPI spec v2.x
```
