/* globals before, after, describe, it */
/*eslint func-names: ["error", "never"]*/
/*eslint no-magic-numbers: "off"*/

"use strict";

const path = require("path");
const assert = require("assert");
const request = require("supertest");

const mock = require("../mock");
const baseUrl = "http://localhost:8080/api";

let server;
let pet1 = {
    "name": "mypet",
    "id": 1
};

before(function (done) {
    mock(path.resolve(__dirname, "example.yaml"), function (mErr, app) {
        if (mErr) {
            done(mErr);
        } else {
            server = app.listen(8080, done);
        }
    });
});

describe("swagger-mockapi", function () {
    it("should export function", function (done) {
        assert.strictEqual(typeof mock, "function");
        assert.strictEqual(mock.length, 2);
        done();
    });
    it("should READ/GET empty pets list", function (done) {
        request(baseUrl)
            .get("/pets")
            .expect(200)
            .expect([], done);
    });
    it("should CREATE/POST pet", function (done) {
        request(baseUrl)
            .post("/pets")
            .set("Content-type", "application/json")
            .send(pet1)
            .expect(200)
            .expect(pet1, done);
    });
    it("should READ/GET pets list", function (done) {
        request(baseUrl)
            .get("/pets")
            .expect(200)
            .expect([pet1], done);
    });
    it("should DELETE/DELETE pet", function (done) {
        request(baseUrl)
            .delete("/pets/1")
            .expect(204, done);
    });
    it("should READ/GET empty pets list", function (done) {
        request(baseUrl)
            .get("/pets")
            .expect(200)
            .expect([], done);
    });
});

after(function (done) {
    server.close();
    done();
});
