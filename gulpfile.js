"use strict";

const gulp = require("gulp");
const eslint = require("gulp-eslint");
const mocha = require("gulp-mocha");

require("gulp-cli")(); // cli output

const paths = {
    "src": ["**/*.js", "!node_modules/**"],
    "test": "test/**.*js"
};

gulp.task("lint", function eslintTask() {
    return gulp
        .src(paths.src)
        .pipe(eslint())
        .pipe(eslint.format())
        .pipe(eslint.failAfterError());
});

gulp.task("test", function testTask() {
    return gulp
        .src(paths.test)
        .pipe(mocha({
            "reporter": "spec"
        }));
});

gulp.task("default", gulp.series("lint", "test"));
