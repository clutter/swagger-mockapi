#!/usr/bin/env node

/* eslint-disable no-console */

"use strict";

const program = require("commander");
const mock = require("./mock");

program
    .version("1.0.0")
    .option("-s, --spec [swagger.yaml]", "Swagger YAML-file", "swagger.yaml")
    .option("-p, --port [port]", "Server port", 8080); // eslint-disable-line no-magic-numbers

program.on("--help", function onHelp() {
    console.log("Notes:");
    console.log("- Supports Swagger/OpenAPI spec v2.x");
});

program.parse(process.argv);

mock(program.spec, function resultCallback(mErr, app) {
    if (mErr) {
        console.log(mErr);
    } else {
        app.listen(program.port, function listenCallback(lErr) {
            if (lErr) {
                console.log(lErr);
            } else {
                console.log(`Mocking API at: http://localhost:${program.port}`);
            }
        });
    }
});
